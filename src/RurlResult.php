<?php
/******************************************************************************************************************
 ** curl 请求结果
******************************************************************************************************************/
namespace PHPTool\Rurl;

class RurlResult
{
    /**
     * 响应的状态码
     */
    public $statusCode;

    /**
     * 错误编码
     */
    public $errorNum = 0;

    /**
     * 错误信息
     */
    public $errorMessage = '';

    /**
     * 请求的uri
     */
    public $uri = '';

    /**
     * 响应 头信息
     */
    public $headers = [];

    /**
     * cookies
     */
    public $cookies = [];

    /**
     * 请求结果内容，出错不设置
     * @var text
     */
    public $contents;

    /**
     * 获取一个header的值
     * @param string 名称
     * @return string
     */
    public function getHeader($name=null)
    {
        if(isset($name))
        {
            if(isset($this->headers[$name]))
            {
                return $this->headers[$name];
            }
            else
            {
                return null;
            }
        }
        else
        {
            return $this->headers;
        }
    }
}

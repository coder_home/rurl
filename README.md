Rurl
=============

对curl的操作进行了封装，可通过对象的方式进行操作。

Methods List
-------------

```
//设置选项(通过数组方式), 同curl的options
$rurl->setOptionArray(array $options);

//设置选项, 同curl的options
$rurl->setOption($option_key, $value);

//设置请求头
$rurl->setRequestHeaderArray(array $headers)

//设置请求头
$rurl->setRequestHeader($header_name, $value)

//设置存储cookie的文件(下次请求会覆盖上次的内容)
$rurl->setCookieJar($filename)

//设置响应头的保存文件
$rurl->setResponseHeaderFile($filename)

//设置包含 cookie 数据的文件（用来发送cookie）
$rurl->setCookieFile($cookie_file);

//设置响应cookie的缓存目录（如果进行了设置，则会自动发送缓存的cookie）
$rurl->setCookieDir($dirname)

//get请求
$rurl->get($url, $param=[])

//post请求
$rurl->post($url, $param=[])

//自定义方式请求
$rurl->request($url, $method, $param=[])

//设置header头解析完成后的回调，返回false则请求失败;回调函数接收两个参数，解析后的header数组 和 响应结果状态码
$rurl->setHeaderFinished($func)

//关闭curl
$rurl->close()
```

